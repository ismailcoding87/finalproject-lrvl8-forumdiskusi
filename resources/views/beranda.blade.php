@extends('layouts.master')

@section('content')
    <div class="row">
        @forelse ($pertanyaan as $key => $item)
            <div class="col-4">
                <a class="text-decoration-none text-white" href=" pertanyaan/{{ $item->id }}">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-uppercase">{{ $item->judul }}</h4>
                            <div class="profile-pic d-flex">
                                <div class="count-indicator">
                                    <img class="img-xs rounded-circle "
                                        src="/image/{{ $item->kategori->user->profile->poto }}" alt="">
                                </div>
                                <div style="margin-left: 8px" class="profile-name">
                                    <h6 class="mb-0 text-uppercase">{{ $item->kategori->user->nama }}</h6>
                                    <span style="font-size: 0.8rem" class="text-muted">Update
                                        {{ date_diff(date_create(substr($item->updated_at, 0, -9)), date_create())->days + 1 }}
                                        hari yang lalu</span>
                                </div>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <div class="btn btn-sm btn-primary mt-2">
                                            <div> {{ $item->kategori->nama }}</div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td valign="bottom">
                                        <div>
                                            <i class="mdi mdi-comment-processing-outline"></i>
                                            {{ $jawaban[$key] }}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </a>
            </div>
        @empty
            <div class="card">
                <div class="card-body">
                    <div class="h2">Belum Ada Pertanyaan</div>
                </div>
            </div>
        @endforelse
    </div>
@endsection
