@extends('layouts.master')
@section('judul')
    Halaman Detail Pertanyaan
@endsection
@section('content')
    <div class="row mt-3">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="h1 text-uppercase">{{ $pertanyaan->judul }}</div>
                    <div class="p-5">
                        {{-- PERTANYAAN --}}
                        <div class="profile-pic d-flex">
                            <div class="count-indicator">
                                <img class="img-xs rounded-circle "
                                    src="/image/{{ $pertanyaan->kategori->user->profile->poto }}" alt="">
                            </div>
                            <div style="margin-left: 8px" class="profile-name">
                                <h5 class="mb-0 text-uppercase text-primary">{{ $pertanyaan->kategori->user->nama }}</h5>
                                <span style="font-size: 0.8rem" class="text-muted">Update
                                    {{ date_diff(date_create(substr($pertanyaan->updated_at, 0, -9)), date_create())->days + 1 }}
                                    hari yang lalu</span>
                            </div>
                        </div>
                        <img class="img-fluid text-center mt-3" src="/image/{{ $pertanyaan->gambar }}" alt="">
                        <div class="fs-5 my-3 text-justify">
                            {{-- {{ htmlspecialchars($pertanyaan->pertanyaan) }} --}}
                            {!! html_entity_decode($pertanyaan->pertanyaan, ENT_QUOTES, 'UTF-8') !!}
                        </div>
                        <div class="btn btn-primary">
                            <div> {{ $pertanyaan->kategori->nama }}</div>
                        </div>

                        {{-- END --}}
                        <hr class="bg-primary mb-5">
                        {{-- JAWABAN --}}
                        @forelse ($jawaban as $key => $item)
                            <div class="profile-pic d-flex my-3">
                                <div class="count-indicator">
                                    <img class="img-xs rounded-circle " src="/image/{{ $item->user->profile->poto }}"
                                        alt="">
                                </div>
                                <div style="margin-left: 8px" class="profile-name">
                                    <h6 class="mb-0 text-uppercase">{{ $item->user->nama }}</h6>
                                    <span style="font-size: 0.8rem" class="text-muted">Update
                                        {{ date_diff(date_create(substr($item->updated_at, 0, -9)), date_create())->days + 1 }}
                                        hari yang lalu</span>
                                </div>
                            </div>
                            <div class="fs-5 text-justify mb-0">
                                {{-- {{ htmlspecialchars($pertanyaan->pertanyaan) }} --}}
                                {!! html_entity_decode($item->jawaban, ENT_QUOTES, 'UTF-8') !!}
                            </div>
                            <hr class="bg-primary mb-1">
                            @if ($user == $item->user_id)
                                <form action="/jawaban/{{ $item->id }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button name="prtnyaan" type="submit" value="{{ $pertanyaan->id }}"
                                        class="btn btn-sm btn-danger">Hapus
                                    </button>
                                </form>
                            @endif
                        @empty
                            <div class="fs-5">Belum Ada Jawaban</div>
                        @endforelse

                        {{-- END --}}
                        {{-- MAKE JAWABAN --}}
                        @if ($user != $pertanyaan->kategori->user_id)
                            <form class="mt-5" action="/jawaban" method="POST">
                                @csrf
                                <div class="form-group mt-3">
                                    <label for="jawaban">Bantu Jawab</label>
                                    <textarea name="jawaban" placeholder="Masukan jawaban"
                                        class="form-control ckeditor @error('jawaban') is-invalid @enderror" id="jawaban" rows="2">{{ old('judul') }}</textarea>
                                    @error('jawaban')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="d-block">
                                    <button name="id" value="{{ $pertanyaan->id }}" type="submit"
                                        class="btn btn-primary w-100">Kirim</button>
                                </div>

                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
