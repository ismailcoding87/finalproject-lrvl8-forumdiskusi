@extends('layouts.app')@section('content')
<style>
    .form-control {
        display: block;
        width: 100%;
        height: 2.875rem;
        padding: 0.56rem 0.75rem;
        font-size: 0.9rem;
        font-weight: 400;
        line-height: 1;
        color: #ffffff;
        background-color: #2A3038;
        background-clip: padding-box;
        border: 1px solid #2c2e33;
        border-radius: 2px;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .form-control:focus {
        outline: none;
        box-shadow: none !important;
        border: 1px solid rgba(71, 164, 71, 0.5);
        background-color: #2A3038;
    }

    .form-control:focus {
        color: #aaaa;
        outline: none;
        box-shadow: none !important;
        border: 1px solid rgba(71, 164, 71, 0.5);
        background-color: #2A3038;
    }

    .form-group label {
        color: #ffffff;
    }

    .icon {
        width: 80px;
        height: 80px;
        background: #0d6efd;
        border-radius: 50%;
        font-size: 30px;
        margin: 0 auto;
        margin-bottom: 10px;
    }

    .login-wrap .icon span {
        color: #fff;
    }
</style>
<div class="container h-100">
    <div class="row h-100 d-flex justify-content-center align-items-center align-content-center mx-1 mx-sm-0">
        <div style="background-color: #191c24" class="col-md-4 col-sm-7 p-md-4 p-sm-3 p-3 rounded-3">
            <form class="p-2" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="fa fa-user-o">
                    </span>
                </div>
                <div class="form-group mb-2 lh-lg">
                    <label for="email">
                        Email
                    </label>
                    <input type="email" class="form-control rounded-2  @error('email') is-invalid @enderror"
                        id="email" placeholder="Masukan Email" name="email" value="{{ old('email') }}" required
                        autocomplete="off" autofocus>
                    </input>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group mb-4 lh-lg">
                        <label for="password">
                            Password
                        </label>
                        <input autocomplete="off" type="text"
                            class="form-control rounded-2  @error('password') is-invalid @enderror" id="password"
                            placeholder="Masukan Password" name="password" value="{{ old('password') }}">
                        </input>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="d-grid mb-3">
                        <button class="btn btn-primary btn-lg rounded-2">
                            Login
                        </button>
                    </div>
                    <div class="fs-6 mb-3 text-white">
                        Belum Punya Akun ?
                        <a href="/register">
                            Registrasi
                        </a>
                    </div>
                </div>
            </form>
        </div>
    @endsection
