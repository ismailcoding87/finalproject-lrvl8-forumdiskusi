@extends('layouts.master')
@section('judul')
    Halaman Kategori
@endsection
@section('content')
    <a href="/kategori/create"class="btn btn-primary">Tambah Kategori</a>
    <div class="row mt-3">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table id="exemple" class="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($kategoris as $key => $item)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>
                                        <form action="/kategori/{{ $item->id }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <a href="/kategori/{{ $item->id }}/edit" class="btn btn-warning">Edit
                                            </a>

                                            <input type="submit" value="Delete" class="btn btn-danger">
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>Data Kategori Kosong</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
