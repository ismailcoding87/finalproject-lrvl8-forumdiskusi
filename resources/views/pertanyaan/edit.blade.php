@extends('layouts.master')
@section('judul')
    Halaman Edit Pertanyaan
@endsection
@section('content')
    <div class="row mt-3">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form class="forms-sample" method="POST" action="/pertanyaan/{{ $pertanyaan->id }}"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="judul">Judul</label>
                            <input name="judul" type="text" class="form-control @error('judul') is-invalid @enderror"
                                id="judul" placeholder="Masukan Judul" value="{{ $pertanyaan->judul }}">
                            @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="pertanyaan">Pertanyaan</label>
                            <textarea name="pertanyaan" placeholder="Masukan Pertanyaan"
                                class="form-control ckeditor @error('pertanyaan') is-invalid @enderror" id="pertanyaan" rows="2">   {!! html_entity_decode($pertanyaan->pertanyaan, ENT_QUOTES, 'UTF-8') !!}</textarea>
                            @error('pertanyaan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName1">Kategori</label>
                            <select name="kategori" class="form-control @error('kategori') is-invalid @enderror"
                                id="kategori">
                                <option selected value="">Pilih Kategori</option>
                                @forelse ($kategori as $key=> $item)
                                    @if ($item->id === $pertanyaan->kategori_id)
                                        <option selected value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @else
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endif
                                @empty
                                @endforelse
                            </select>
                            @error('kategori')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <input id="file" type="file" name="gambar" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text"
                                    class="form-control file-upload-info @error('gambar') is-invalid @enderror" disabled
                                    placeholder="Upload Gambar">
                                <span class="input-group-append">
                                    <button id="click-file"
                                        class="file-upload-browse btn btn-primary @error('gambar') border-danger @enderror"
                                        type="button">Upload</button>
                                </span>
                                @error('gambar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-dark">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
