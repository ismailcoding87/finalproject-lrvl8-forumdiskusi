@extends('layouts.master')

@section('judul')
    Ubah Profile
@endsection
@section('content')
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="umur">Nama</label>
                        <input style="background-color: #1e2127" disabled class="form-control" type="text"
                            value="{{ $profile->user->nama }}">
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input name="umur" autocomplete="off" type="number" class="form-control" id="umur"
                            placeholder="Masukan Umur" value="{{ $profile->umur }}">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea autocomplete="off" name="alamat" placeholder="Masukan Alamat" class="form-control" id="alamat"
                            rows="2">{{ $profile->alamat }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="biodata">Biodata</label>
                        <textarea autocomplete="off" name="biodata" placeholder="Masukan Bio" class="form-control" id="biodata"
                            rows="2">{{ $profile->biodata }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="poto">Poto</label>
                        <input id="file" type="file" name="poto" class="file-upload-default">
                        <div class="input-group col-xs-12">
                            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Poto">
                            <span class="input-group-append">
                                <button id="click-file" class="file-upload-browse btn btn-primary"
                                    type="button">Upload</button>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button class="btn btn-dark" type="reset">Batal</button>
                </form>
            </div>
        </div>
    </div>
@endsection
