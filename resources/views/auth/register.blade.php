@extends('layouts.app')

@section('content')
    <div class="row w-100 m-0">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
            <div class="card col-lg-4 mx-auto">
                <div class="card-body px-5 py-5">
                    <h3 class="card-title text-left mb-3">Register</h3>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input autocomplete="off" name="nama" value="{{ old('nama') }}" type="text"
                                class="form-control p_input  @error('nama') is-invalid @enderror" id="nama"
                                placeholder="Masukan nama">
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input autocomplete="off" name="umur" min="1" type="number"
                                value="{{ old('umur') }}"
                                class="form-control rounded-2 @error('umur') is-invalid @enderror" id="umur"
                                placeholder="Masukan Umur">
                            @error('umur')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea autocomplete="off" name="alamat" id="alamat" placeholder="Masukan Alamat"
                                class="form-control rounded-2 @error('alamat') is-invalid @enderror">{{ old('alamat') }}</textarea>
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="biodata">Biodata</label>
                            <textarea autocomplete="off" name="biodata" id="biodata" placeholder="Masukan Biodata"
                                class="form-control rounded-2 @error('biodata') is-invalid @enderror">{{ old('biodata') }}</textarea>
                            @error('biodata')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input autocomplete="off" name="email" value="{{ old('email') }}" type="text"
                                class="form-control rounded-2 @error('email') is-invalid @enderror" id="email"
                                placeholder="Masukan Email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input autocomplete="off" name="password" value="{{ old('password') }}" type="password"
                                class="form-control rounded-2 @error('password') is-invalid @enderror" id="password"
                                placeholder="Masukan Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="text-center mt-3">
                            <button type="submit" class="btn btn-primary btn-block enter-btn">Registrasi</button>
                        </div>
                        <p class="sign-up text-left">Sudah punya Account ?<a href="/login"> Login</a></p>
                    </form>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
@endsection
