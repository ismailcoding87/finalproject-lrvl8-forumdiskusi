@extends('layouts.app')@section('content')
<div class="row w-100 m-0">
    <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
        <div class="card col-lg-4 mx-auto">
            <div class="card-body px-5 py-5">
                <h3 class="card-title text-left mb-3">Login</h3>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label>Email *</label>
                        <input type="email" class="form-control p_input  @error('email') is-invalid @enderror"
                            id="email" placeholder="Masukan Email" name="email" value="{{ old('email') }}"
                            required autocomplete="off" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Password *</label>
                        <input autocomplete="off" type="text"
                            class="form-control p_input  @error('password') is-invalid @enderror" id="password"
                            placeholder="Masukan Password" name="password" value="{{ old('password') }}">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group d-flex align-items-center justify-content-between">
                        <div class="form-check">
                            <div> <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>
                                <span href="#" class="forgot-pass">Ingatkan</span>
                            </div>
                        </div>
                        <a href="#" class="forgot-pass">Forgot password</a>
                    </div>
                    <div class="text-start">
                        <button type="submit" class="btn btn-primary btn-block enter-btn">Login</button>
                    </div>
                    <p class="sign-up text-left">Belum Punya account ? <a href="/register">Registrasi</a></p>
                </form>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
</div>
@endsection
