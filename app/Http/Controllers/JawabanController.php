<?php

namespace App\Http\Controllers;

use App\Models\Jawaban;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'jawaban' => 'required',
        ]);
        $jawaban = new Jawaban();
        $jawaban->jawaban = $request->jawaban;
        $jawaban->user_id = Auth::id();
        $jawaban->pertanyaan_id = $request->id;
        $jawaban->save();
        return redirect("/pertanyaan/$request->id")->with("succsess", "Data Berhasil Di Simpan !");
    }
    public function destroy(Request $request, $id)
    {
        $jawaban = Jawaban::find($id);
        $jawaban->delete();
        return redirect("/pertanyaan/$request->prtnyaan")->with("succsess", "Data Berhasil Di Hapus !");
    }
}
