@extends('layouts.master')
@section('judul')
    Halaman Edit Kategori
@endsection
@section('content')
    <div class="row mt-3">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="/kategori/{{ $kategori->id }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" name='nama' value="{{ $kategori->nama }}" class="form-control">
                        </div>
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
