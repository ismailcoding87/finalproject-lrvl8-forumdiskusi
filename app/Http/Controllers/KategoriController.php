<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use Illuminate\Support\Facades\Auth;
// Traits
use App\Traits\GeneralTrait;


class KategoriController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::where('user_id', Auth::id())->get();
        $data = ['kategoris' => $kategori, "poto" => $this->potoProfile(), "routeRequest" => $this->routeRequest()];
        return view('kategori.tampil', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ["poto" => $this->potoProfile(), "routeRequest" => $this->routeRequest()];
        return view('kategori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);
        $kategori = new Kategori();
        $kategori->nama = $request->nama;
        $kategori->user_id = Auth::id();
        $kategori->save();
        return redirect('/kategori')->with("succsess", "Data Berhasil Di Simpan !");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = Kategori::find($id);
        $data = ['kategori' => $kategori, "poto" => $this->potoProfile(Auth::id()), "routeRequest" => $this->routeRequest()];
        return view('kategori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);
        $kategori = Kategori::find($id);
        $kategori->nama = $request["nama"];
        // 
        $kategori->save();

        return redirect('/kategori')->with("succsess", "Data Berhasil Di Ubah !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Kategori::find($id);
        $post->delete();
        return redirect('/kategori')->with("succsess", "Data Berhasil Di hapus !");
    }
}
