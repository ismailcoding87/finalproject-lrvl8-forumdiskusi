<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;
    protected $table = "jawaban";
    protected $fillable = ["jawaban", "user_id", "pertanyaan_id"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function jawaban()
    {
        return $this->belongsTo(Jawaban::class, 'pertanyaan_id');
    }
}
