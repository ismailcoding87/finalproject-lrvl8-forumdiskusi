<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use App\Models\Jawaban;
use App\Models\User;
// Traits
use App\Traits\GeneralTrait;

class PertanyaanController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = User::select(
            "kategori.nama as kat_nama",
            "pertanyaan.id",
            "pertanyaan.judul",
            "pertanyaan.pertanyaan",
        )->where("users.id", Auth::id())
            ->join("kategori", "kategori.user_id", "=", "users.id")
            ->join("pertanyaan", "pertanyaan.kategori_id", "=", "kategori.id")
            ->get();

        $data = ["pertanyaan" => $pertanyaan, "poto" => $this->potoProfile(Auth::id()), "routeRequest" => $this->routeRequest()];
        return view("pertanyaan.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $kategori = Kategori::where('user_id', Auth::id())->get();
        $data = ['kategori' => $kategori, "poto" => $this->potoProfile(Auth::id()), "routeRequest" => $this->routeRequest()];
        return view("pertanyaan.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'judul' => 'required',
            'pertanyaan' => 'required',
            'kategori' => 'required',
            'gambar' => 'required'
        ]);
        $pertanyaan = new Pertanyaan;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->pertanyaan = $request->pertanyaan;
        // 
        $fileName = time() . '.' . $request->gambar->extension();
        $request->gambar->move(public_path("image"), $fileName);
        $pertanyaan->gambar = $fileName;
        // 
        $pertanyaan->kategori_id = $request->kategori;
        $pertanyaan->save();
        return redirect('/pertanyaan')->with("succsess", "Data Berhasil Di Simpan !");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::id();
        $pertanyaan = Pertanyaan::find($id);
        $jawaban = Jawaban::where("pertanyaan_id", $id)->get();
        $data = ['user' => $user, 'pertanyaan' => $pertanyaan, 'jawaban' => $jawaban, "poto" => $this->potoProfile(Auth::id()), "routeRequest" => $this->routeRequest()];
        return view("pertanyaan.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::where('user_id', Auth::id())->get();
        $pertanyaan = Pertanyaan::find($id);
        $data = ["pertanyaan" => $pertanyaan, "kategori" => $kategori, "poto" => $this->potoProfile(), "routeRequest" => $this->routeRequest()];
        return view("pertanyaan.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'pertanyaan' => 'required',
            'kategori' => 'required',
        ]);
        // 
        $pertanyaan = Pertanyaan::find($id);
        // 
        $pertanyaan->judul = $request["judul"];
        $pertanyaan->pertanyaan = $request["pertanyaan"];
        if ($request->has("gambar")) {
            $path = "image/";
            if (file_exists(public_path($path . $pertanyaan->gambar)) && $pertanyaan->gambar != "user.png") {
                unlink(public_path($path . $pertanyaan->gambar));
            }
            // 
            $fileName = time() . '.' . $request->gambar->extension();
            $request->gambar->move(public_path("image"), $fileName);
            $pertanyaan->gambar = $fileName;
        }
        $pertanyaan->kategori_id = $request["kategori"];
        // 
        $pertanyaan->save();
        return redirect('/pertanyaan')->with("succsess", "Data Berhasil Di Update !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pertanyaan = Pertanyaan::find($id);
        $path = "image/";
        if (file_exists(public_path($path . $pertanyaan->gambar)) && $pertanyaan->gambar != "user.png") {
            unlink(public_path($path . $pertanyaan->gambar));
        }
        $pertanyaan->delete();
        return redirect('/pertanyaan')->with("succsess", "Data Berhasil Di Hapus !");
    }
}
