@extends('layouts.master')
@section('judul')
    Halaman Pertanyaan
@endsection
@section('content')
    <a href="pertanyaan/create" class="btn btn-primary">Tambah Pertanyaan</a>
    <div class="row mt-3">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th> No </th>
                                    <th> Judul </th>
                                    <th>Pertanyaan</th>
                                    <th>Kategori</th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($pertanyaan as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->judul }}</td>
                                        <td>
                                            {!! Str::limit(strip_tags($item->pertanyaan), 10) !!}
                                        </td>
                                        <td>
                                            {{ $item->kat_nama }}
                                        </td>
                                        <td>
                                            <form action="/pertanyaan/{{ $item->id }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <a href="/pertanyaan/{{ $item->id }}" class="btn btn-info">Detail
                                                </a>
                                                <a href="/pertanyaan/{{ $item->id }}/edit" class="btn btn-warning">Edit
                                                </a>
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
