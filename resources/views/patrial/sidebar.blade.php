 <nav class="sidebar sidebar-offcanvas" id="sidebar">
     <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
         <a class="sidebar-brand brand-logo" href="index.html"><img src="/assets/images/logo.svg" alt="logo" /></a>
         <a class="sidebar-brand brand-logo-mini" href="index.html"><img src="/assets/images/logo-mini.svg"
                 alt="logo" /></a>
     </div>
     <ul class="nav">
         <li class="nav-item profile">
             @auth
                 <div class="profile-desc">
                     <div class="profile-pic">
                         <div class="count-indicator">
                             <img class="img-xs rounded-circle " src="/image/{{ $poto }}" alt="">
                             <span class="count bg-success"></span>
                         </div>
                         <div class="profile-name">
                             <h5 class="mb-0 font-weight-normal">{{ Auth::user()->nama }}</h5>
                             <span>Pengguna</span>
                         </div>
                     </div>
                     <a href="#" id="profile-dropdown" data-toggle="dropdown"><i
                             class="mdi mdi-dots-vertical"></i></a>
                     <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list"
                         aria-labelledby="profile-dropdown">
                         <div class="dropdown-divider"></div>
                         <a class="dropdown-item preview-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                             <div class="preview-thumbnail">
                                 <div class="preview-icon bg-dark rounded-circle">
                                     <i class="mdi mdi-onepassword  text-info"></i>
                                 </div>
                             </div>
                             <div class="preview-item-content">
                                 <p class="preview-subject ellipsis mb-1 text-small">Logout</p>
                             </div>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                 @csrf
                             </form>
                         </a>
                         <div class="dropdown-divider"></div>
                     </div>
                 </div>
             @endauth
         </li>
         <li class="nav-item nav-category">
             <span class="nav-link">Navigation</span>
         </li>
         <li class="nav-item menu-items {{ $routeRequest == '/' ? 'active' : '' }}">
             <a class="nav-link" href="/">
                 <span class="menu-icon">
                     <i class="mdi mdi-speedometer"></i>
                 </span>
                 <span class="menu-title">Beranda</span>
             </a>
         </li>
         @auth
             <li
                 class="nav-item menu-items  {{ $routeRequest == 'pertanyaan' || $routeRequest == 'pertanyaan/create' || $routeRequest == 'pertanyaan/edit' ? 'active' : '' }}">
                 <a class="nav-link" href="/pertanyaan">
                     <span class="menu-icon">
                         <i class="mdi mdi-speedometer"></i>
                     </span>
                     <span class="menu-title">Pertanyaan</span>
                 </a>
             </li>
         @endauth
         @auth
             <li
                 class="nav-item menu-items {{ $routeRequest == 'kategori' || $routeRequest == 'kategori/create' || $routeRequest == 'kategori/edit' ? 'active' : '' }}">
                 <a class="nav-link" href="/kategori">
                     <span class="menu-icon">
                         <i class="mdi mdi-speedometer"></i>
                     </span>
                     <span class="menu-title">Kategori</span>
                 </a>
             </li>
         @endauth
         @auth
         @endauth
         <li class="nav-item menu-items {{ $routeRequest == 'profile' ? 'active' : '' }}">
             <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                 <span class="menu-icon">
                     <i class="mdi mdi-security"></i>
                 </span>
                 <span class="menu-title">Halaman User</span>
                 <i class="menu-arrow"></i>
             </a>
             <div class=" {{ $routeRequest == 'profile' ? '' : 'collapse' }}" id="auth">
                 <ul class="nav flex-column sub-menu ">
                     @auth
                         <li class="nav-item"> <a class="nav-link {{ $routeRequest == 'profile' ? 'active' : '' }}"
                                 href="/profile"> Profile </a></li>
                     @endauth
                     @guest
                         <li class="nav-item"> <a class="nav-link" href="/login"> Login </a></li>
                     @endguest
                     @guest
                         <li class="nav-item"> <a class="nav-link" href="/register"> Register </a></li>
                     @endguest
                 </ul>
             </div>
         </li>
     </ul>
 </nav>
