<?php

namespace App\Traits;

use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * 
 */
trait GeneralTrait
{
 public function potoProfile()
 {
  if (!empty(Auth::id())) {
   $result = Profile::where('user_id', Auth::id())->first();
   if (empty($result)) {
    $profile = "";
   } else {
    $profile = $result["poto"];
   }
  } else {
   $profile = "";
  }
  return $profile;
 }

 public function routeRequest()
 {
  $url = explode("/", url()->current());
  $longUrl = count($url);
  if ($longUrl == 3) {
   $requestUrl = "/";
  } elseif ($longUrl == 4) {
   $requestUrl = $url[3];
  } elseif ($longUrl == 5) {
   $requestUrl = $url[3];
  } elseif ($longUrl == 6) {
   $requestUrl = $url[3] . "/" . $url[5];
  } else {
   $requestUrl = "";
  }
  return $requestUrl;
 }
}
