<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Pertanyaan;
// Traits
use App\Traits\GeneralTrait;

class BerandaController extends Controller
{
    use GeneralTrait;
    public function index()
    {

        $pertanyaan = Pertanyaan::get();
        $jawaban = [];
        foreach ($pertanyaan as $key => $value) {
            array_Push($jawaban, $this->sumJawaban($value->id));
        }
        print_r($jawaban);
        $data = ["pertanyaan" => $pertanyaan, "jawaban" => $jawaban, "poto" => $this->potoProfile(), "routeRequest" => $this->routeRequest()];
        return view("beranda", $data);
    }
    public function sumJawaban($id)
    {
        $pertanyaan = Pertanyaan::find($id)->jawaban->count('pertanyaan_id');
        return $pertanyaan;
    }
}
