@extends('layouts.app')

@section('content')
    <style>
        .form-control {
            display: block;
            width: 100%;
            height: 2.875rem;
            padding: 0.56rem 0.75rem;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1;
            color: #ffffff;
            background-color: #2A3038;
            background-clip: padding-box;
            border: 1px solid #2c2e33;
            border-radius: 2px;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .form-control:focus {
            outline: none;
            box-shadow: none !important;
            border: 1px solid rgba(71, 164, 71, 0.5);
            background-color: #2A3038;
        }

        .form-control:focus {
            color: #aaaa;
            outline: none;
            box-shadow: none !important;
            border: 1px solid rgba(71, 164, 71, 0.5);
            background-color: #2A3038;
        }

        .form-group label {
            color: #ffffff;
        }

        textarea {
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }

        textarea {
            overflow: auto;
            resize: vertical;
        }
    </style>
    <div class="container h-100">
        <div class="row h-100 d-flex justify-content-center align-items-center align-content-center mx-1 mx-sm-0">
            <div style="background-color: #191c24" class="col-md-4 col-sm-7 p-md-4 p-sm-3 p-3 rounded-3">
                <form class="forms-sample" class="p-2" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group mb-1 lh-lg">
                        <label for="nama">Nama</label>
                        <input autocomplete="off" name="nama" value="{{ old('nama') }}" type="text"
                            class="form-control rounded-2  @error('nama') is-invalid @enderror" id="nama"
                            placeholder="Masukan nama">
                        @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-1 lh-lg">
                        <label for="umur">Umur</label>
                        <input autocomplete="off" name="umur" min="1" type="number" value="{{ old('umur') }}"
                            class="form-control rounded-2 @error('umur') is-invalid @enderror" id="umur"
                            placeholder="Masukan Umur">
                        @error('umur')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-1 lh-lg">
                        <label for="alamat">Alamat</label>
                        <textarea autocomplete="off" name="alamat" id="alamat" placeholder="Masukan Alamat"
                            class="form-control rounded-2 @error('alamat') is-invalid @enderror">{{ old('alamat') }}</textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-1 lh-lg">
                        <label for="biodata">Biodata</label>
                        <textarea autocomplete="off" name="biodata" id="biodata" placeholder="Masukan Biodata"
                            class="form-control rounded-2 @error('biodata') is-invalid @enderror">{{ old('biodata') }}</textarea>
                        @error('biodata')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-1 lh-lg">
                        <label for="email">Email</label>
                        <input autocomplete="off" name="email" value="{{ old('email') }}" type="text"
                            class="form-control rounded-2 @error('email') is-invalid @enderror" id="email"
                            placeholder="Masukan Email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group mb-3 lh-lg">
                        <label for="password">Password</label>
                        <input autocomplete="off" name="password" value="{{ old('password') }}" type="text"
                            class="form-control rounded-2 @error('password') is-invalid @enderror" id="password"
                            placeholder="Masukan Password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="d-grid mb-3">
                        <button class="btn btn-primary btn-lg rounded-2">Registrasi</button>
                    </div>
                </form>
                <div class="fs-6 text-white">Sudah Punya Akun ? <a href="/login">Login</a></div>

            </div>
        </div>
    </div>
@endsection
