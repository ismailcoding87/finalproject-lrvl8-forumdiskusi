<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

use Illuminate\Support\Facades\Auth;
// Traits
use App\Traits\GeneralTrait;

class ProfileController extends Controller
{
    use GeneralTrait;
    public function index()
    {
        $idUser = Auth::id();
        $detailProfile = Profile::find($idUser);
        // print_r($detailProfile->user->nama);
        $data = ["profile" => $detailProfile, "poto" => $this->potoProfile(), "routeRequest" => $this->routeRequest()];
        return view("profile.index", $data);
    }
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);

        if ($request->has("poto")) {
            $path = "image/";
            if (file_exists(public_path($path . $profile->poto)) && $profile->poto != "user.png") {
                unlink(public_path($path . $profile->poto));
            }
            // 
            $fileName = time() . '.' . $request->poto->extension();
            $request->poto->move(public_path("image"), $fileName);
            $profile->poto = $fileName;
        }
        $profile->umur = $request["umur"];
        $profile->alamat = $request["alamat"];
        $profile->biodata = $request["biodata"];
        // 
        $profile->save();
        return redirect("/")->with("succsess", "Data Berhasil Di Ubah !");
    }
}
