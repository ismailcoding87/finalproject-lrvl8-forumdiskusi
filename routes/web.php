<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Beranda;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\JawabanController;
// Auth

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Beranda
Route::get('/', [BerandaController::class, 'index']);
// Auth
Auth::routes();

// Profile
Route::middleware(['auth'])->group(
 function () {
  Route::resource("profile", ProfileController::class)->only(["index", "update"]);
 }
);
//Kategori CRUD
Route::middleware(['auth'])->group(
 function () {
  Route::resource("kategori", KategoriController::class);
 }
);
// //CREATE DATA
// Route::get('/kategori/create', [KategoriController::class, 'create']);
// //Posting Kategori
// Route::post('/kategori', [KategoriController::class, 'post']);

// //Read
// Route::get('/kategori', [KategoriController::class, 'index']);

// //Detail
// Route::get('/kategori/{id}', [KategoriController::class, 'show']);

// //Updaet

// Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);

// //Edit Data
// Route::put('/kategori/{id}', [KategoriController::class, 'update']);

// //Delete Data
// Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);

// Pertanyaan CRUD
Route::middleware(['auth'])->group(
 function () {
  Route::resource("pertanyaan", PertanyaanController::class);
 }
);
// Jawaban
Route::middleware(['auth'])->group(
 function () {
  Route::resource("jawaban", JawabanController::class)->only(["store", "destroy"]);
 }
);
